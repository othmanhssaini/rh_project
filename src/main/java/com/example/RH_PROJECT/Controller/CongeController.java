package com.example.RH_PROJECT.Controller;
import com.example.RH_PROJECT.DTO.CongeDTO;
import com.example.RH_PROJECT.Service.CongeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class CongeController {

    @Autowired
    private CongeService congeService;

    @GetMapping("/conges")
    public List<CongeDTO> getCongesDTO(){
        return congeService.getCongesDTO();
    }

    @GetMapping("/conge/{id}")
    public ResponseEntity<CongeDTO> getCongeByIdDTO(@PathVariable Long id ){
        return congeService.getCongeByIdDTO(id);
    }

    @PostMapping("/conge")
    public ResponseEntity<CongeDTO> createCongeDTO(@RequestBody CongeDTO congeDTO){
        return congeService.createCongeDTO(congeDTO);
    }

    @PutMapping("/conge/{id}")
    public ResponseEntity<CongeDTO> updateCongeDTO(@PathVariable Long id , @RequestBody CongeDTO congeDTO)  {
        return congeService.updateCongeDTO(id , congeDTO);
    }

    @DeleteMapping("/conge/{id}")
    public void deleteCongeDTO(@PathVariable Long id ){
        congeService.deleteCongeDTO(id);
    }
}
