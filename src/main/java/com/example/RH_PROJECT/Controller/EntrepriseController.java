package com.example.RH_PROJECT.Controller;
import com.example.RH_PROJECT.Model.Entreprise;
import com.example.RH_PROJECT.Service.EntrepriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class EntrepriseController {

    @Autowired
    private EntrepriseService entrepriseService ;

    @GetMapping("/entreprise")
    public List<Entreprise> getAllDepartement(){
        return entrepriseService.getEntreprises();
    }

    @GetMapping("/entreprise/{id}")
    public Entreprise getEntrepriseById(@PathVariable(name = "id") Long id){
        return entrepriseService.getEntrepriseById(id);
    }

    @PostMapping("/entreprise")
    public Entreprise createEntreprise(@RequestBody Entreprise entreprise){
        return entrepriseService.createEntreprise(entreprise);
    }

    @PutMapping("/entreprise/{id}")
    public Entreprise updateEntreprise(@PathVariable long id, @RequestBody Entreprise entreprise){
        return entrepriseService.updateEntreprise(id,entreprise);
    }

    @DeleteMapping("/entreprise/{id}")
    public void  deleteEntreprise(@PathVariable(name = "id") Long id){
        entrepriseService.deleteEntreprise(id);
    }
}
