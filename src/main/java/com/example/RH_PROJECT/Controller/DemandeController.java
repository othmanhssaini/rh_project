package com.example.RH_PROJECT.Controller;

import com.example.RH_PROJECT.DTO.DemandeDTO;
import com.example.RH_PROJECT.Model.Demande;
import com.example.RH_PROJECT.Model.Document;
import com.example.RH_PROJECT.Service.DemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.metamodel.ListAttribute;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class DemandeController {

  @Autowired
  private DemandeService demandeService ;

  @GetMapping("/demande")
  public List<DemandeDTO> getAllDemandes(){
    return demandeService.getDemandesDTO();
  }

  @GetMapping("/demande/{id}")
  public ResponseEntity<DemandeDTO> getDemandeById(@PathVariable long id){
    return demandeService.getDemandeByIdDTO(id);
  }

  @PostMapping("/demande")
  public ResponseEntity<DemandeDTO> createDemande(@RequestBody DemandeDTO demandeDTO){
    return demandeService.createDemandeDTO(demandeDTO);
  }

  @PutMapping("/demande/{id}")
  public ResponseEntity<DemandeDTO> updateDemande(@RequestBody DemandeDTO demandeDTO, @PathVariable long id ){
    return demandeService.updateDemandeDTO(id,demandeDTO);
  }

  @DeleteMapping("/demande/{id}")
  public void deleteDemandeDTO(@PathVariable long id){
     demandeService.deleteDemandeDTO(id);
  }

  @PutMapping("/demande/{demandeId}/user/{userId}")
  public Demande addDemandeToUser(
    @PathVariable long demandeId,
    @PathVariable long userId
  ){
    return demandeService.addDemandeToUser(userId , demandeId);
  }


}
