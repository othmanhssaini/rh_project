package com.example.RH_PROJECT.Controller;
import com.example.RH_PROJECT.DTO.UserDTO;
import com.example.RH_PROJECT.Model.Conge;
import com.example.RH_PROJECT.Model.User;
import com.example.RH_PROJECT.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class UserController {

    @Autowired
    private UserService userService ;

    @GetMapping("/user")
    public List<User> getAllDepartement(){
        return userService.getUsers();
    }

    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable(name = "id") Long id){
        return userService.getUserById(id);
    }

    @PostMapping("/user")
    public User createUser(@RequestBody User user){
        return userService.createUser(user);
    }

    @PutMapping("/user/{id}")
    public User updateUser(@PathVariable long id, @RequestBody User user){
        return userService.updateUser(id,user);
    }

    @DeleteMapping("/user/{id}")
    public void  deleteUser(@PathVariable(name = "id") Long id){
        userService.deleteUserDTO(id);
    }

    @PutMapping("/departement/{depratementId}/user/{userId}")
    public User adddUserToDepartements(
        @PathVariable Long depratementId ,
        @PathVariable Long userId
        ) {
    return userService.addUserToDepartement(depratementId , userId);
    }

    @GetMapping("/user/disponibles")
    public List<User> getUsersDisponibles(){
        return userService.getUsersDisponibles();
    }

    @GetMapping("/user/manager/disponibles")
    public List<User> getManagerDisponibles(){
        return userService.getManagerDisponibles();
    }

    @PatchMapping("/user/desaffecte/{userId}")
    public User desaffectManager(@PathVariable Long userId,@RequestBody User user){
        return userService.desaffectManager(userId,user);
    }

    @GetMapping("/user/info")
    public User getUserDetails(){
        return userService.getUserInfo();
    }
}
