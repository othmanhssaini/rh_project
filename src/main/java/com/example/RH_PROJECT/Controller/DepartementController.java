package com.example.RH_PROJECT.Controller;
import com.example.RH_PROJECT.DTO.AffectUsers;
import com.example.RH_PROJECT.DTO.DepartementDTO;
import com.example.RH_PROJECT.Model.Departement;
import com.example.RH_PROJECT.Service.DepartementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class DepartementController {

    @Autowired
    private DepartementService departementService ;

    @GetMapping("/departements")
    public List<DepartementDTO> getAllDepartement(){
        return departementService.getDepartementsDTO();
    }

    @GetMapping("/departement/{id}")
    public Departement getDepartementById(@PathVariable(name = "id") Long id){
        return departementService.getDepartementById(id);
    }

    @PostMapping("/departement")
    public Departement  createDepartement(@RequestBody Departement departement){
        return departementService.createDepartement(departement);
    }

    @PutMapping("/departement/{id}")
    public Departement updateDepartement(@PathVariable long id, @RequestBody Departement departement){
        return departementService.updateDepartement(id,departement);
    }

    @DeleteMapping("/departement/{id}")
    public void  deleteDepartement(@PathVariable(name = "id") Long id){
        departementService.deleteDepatementDTO(id);
    }

    @PutMapping("/departement/{departementId}/entreprise/{entrepriseId}")
    public Departement assignEntrepriseToDepartement(@PathVariable Long departementId , @PathVariable Long entrepriseId ){
        return departementService.assignEntrepriseToDepartement(departementId, entrepriseId );
    }

    @PutMapping("/departement/{depratementId}/affectUsers")
    public Departement adddUsersToDepartements(
            @PathVariable Long depratementId ,
            @RequestBody AffectUsers affectUsers
    ) {
        return departementService.addUsersToDepartement(depratementId , affectUsers.getUserIds());
    }
}
