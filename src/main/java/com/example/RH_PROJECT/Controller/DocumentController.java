package com.example.RH_PROJECT.Controller;

import com.example.RH_PROJECT.DTO.DocumentDTO;
import com.example.RH_PROJECT.Service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")

public class DocumentController {

  @Autowired
  public DocumentService documentService ;

  @GetMapping("/document")
  public List<DocumentDTO> getAllDocument(){
    return documentService.getDocumentsDTO();
  }

  @GetMapping("/document/{id}")
  public ResponseEntity<DocumentDTO> getDocumentDTOById(@PathVariable long id){
    return documentService.getDocumentDTOById(id);
  }

  @PostMapping("/document")
  public ResponseEntity<DocumentDTO> createDocument(@RequestBody DocumentDTO documentDTO){
    return documentService.createDocumentDTO(documentDTO);
  }

  @PutMapping("/document/{id}")
  public ResponseEntity<DocumentDTO> updateDocument(@PathVariable long id , @RequestBody DocumentDTO documentDTO){
    return documentService.updateDocumentDTO(id,documentDTO);
  }

  @DeleteMapping("/document/{id}")
  public void deleteDocumentDTO(@PathVariable long id){
    documentService.deleteDocumentDTO(id);
  }
}
