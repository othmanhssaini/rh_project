package com.example.RH_PROJECT.Service;
import com.example.RH_PROJECT.DTO.DepartementDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Departement;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface IDepartementService {

    public List<Departement> getDepartements();

    Departement getDepartementById(Long id);

    Departement createDepartement(Departement departement);

    Departement updateDepartement(Long id, Departement d);

    void deleteDepartement(Long id) throws ResourceNotFoundException;

    Departement assignEntrepriseToDepartement(Long departementId, Long entrepriseId);

    Departement addUsersToDepartement(Long departementId, Long[] userIds);

    List<DepartementDTO> getDepartementsDTO();

    ResponseEntity<DepartementDTO> getDepartementsByIdDTO(Long id);

    ResponseEntity<DepartementDTO> createDepartementDTO(DepartementDTO departementDTO);

    ResponseEntity<DepartementDTO> updateDepartementDTO(Long id, DepartementDTO departementDTO);

    void  deleteDepatementDTO(Long id);

}
