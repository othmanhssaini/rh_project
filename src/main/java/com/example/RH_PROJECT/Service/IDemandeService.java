package com.example.RH_PROJECT.Service;

import com.example.RH_PROJECT.DTO.DemandeDTO;
import com.example.RH_PROJECT.Model.Demande;
import com.example.RH_PROJECT.Model.Document;
import org.springframework.http.ResponseEntity;

import javax.persistence.metamodel.ListAttribute;
import java.util.List;

public interface IDemandeService {

  List<Demande> getDemandes();

  List<DemandeDTO> getDemandesDTO();

  Demande getDemandeById(Long id);

  ResponseEntity<DemandeDTO> getDemandeByIdDTO(Long id);

  Demande createDemande(Demande demande);

  Demande updateDemande(Long id, Demande demande);

  void deleteDemande(Long id);

  void deleteDemandeDTO(Long id);

  Demande addDemandeToUser(Long userId, Long demandeId);

  ResponseEntity<DemandeDTO> createDemandeDTO(DemandeDTO demandeDTO);

  ResponseEntity<DemandeDTO> updateDemandeDTO(Long id, DemandeDTO demandeDTO);
}
