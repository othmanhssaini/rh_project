package com.example.RH_PROJECT.Service;
import com.example.RH_PROJECT.DAO.UserRepository;
import com.example.RH_PROJECT.DAO.DepartementRepository;
import com.example.RH_PROJECT.DTO.UserDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Conge;
import com.example.RH_PROJECT.Model.Departement;
import com.example.RH_PROJECT.Model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService{

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final DepartementRepository departementRepository;

    @Autowired
    private ModelMapper modelMapper ;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, DepartementRepository departementRepository) {
        this.userRepository = userRepository;
      this.departementRepository = departementRepository;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("User not found on :: " +id));
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User updateUser(Long id, User u) {
        User user = userRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("User not found on :: " +id));
        user.setFirstName(u.getFirstName());
        user.setLastName(u.getLastName());
        user.setDob(u.getDob());
        user.setAge(u.getAge());
        user.setEmail(u.getEmail());
        user.setPhoneNumber(u.getPhoneNumber());
        user.setAddress(u.getAddress());
        user.setDateEntree(u.getDateEntree());
        user.setDateSortie(u.getDateSortie());
        user.setUserRole(u.getUserRole());
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) throws ResourceNotFoundException {
        User user = userRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("User not found on :: " +id));
        userRepository.delete(user);
    }

    @Override
    public User addUserToDepartement(Long departementId, Long userId) {
    Departement departement = departementRepository.findById(departementId).
      orElseThrow(()-> new ResourceNotFoundException("Departement not found : " +departementId) );
    User user = userRepository.findById(userId).
      orElseThrow(()-> new ResourceNotFoundException("User not found " +userId));
      user.addUserToDepartement(departement);
    return userRepository.save(user);
    }

    @Override
    public List<User> getUsersDisponibles() {
        return userRepository.getUsersDisponibles();
    }

    @Override
    public List<User> getManagerDisponibles() {
        return userRepository.getManagerDisponibles();
    }

    @Override
    public User desaffectManager(Long id, User u) {
        User user = userRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("User not found on :: " +id));
        user.setDepartement(null);
        return userRepository.save(user);
    }


    @Override
    public List<UserDTO> getUsersDTO(){
        return this.getUsers().stream()
                .map(user -> modelMapper.map(user,UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<UserDTO> getUsersByIdDTO(Long id){
        User user = this.getUserById(id);
        UserDTO userDTO = modelMapper.map(user,UserDTO.class);
        return ResponseEntity.ok().body(userDTO);
    }

    @Override
    public ResponseEntity<UserDTO> createUserDTO(UserDTO userDTO){
        User userRequest = modelMapper.map(userDTO,User.class);
        User user = this.createUser(userRequest);
        String encodedPass = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPass);
        UserDTO userResponse = modelMapper.map(user , UserDTO.class);
        return new ResponseEntity<>(userResponse , HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<UserDTO> updateUserDTO(Long id, UserDTO userDTO){
        User userRequest = modelMapper.map(userDTO , User.class);
        User user = this.updateUser(id,userRequest);
        UserDTO userResponse = modelMapper.map(user , UserDTO.class);
        return ResponseEntity.ok().body(userResponse);
    }

    @Override
    public  void  deleteUserDTO(Long id){
        this.deleteUser(id);
    }

    @Override
    public User getUserInfo(){
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByEmail(email).get();
    }
}
