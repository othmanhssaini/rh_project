package com.example.RH_PROJECT.Service;
import com.example.RH_PROJECT.DTO.EntrepriseDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Entreprise;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface IEntrepriseService {

    public List<Entreprise> getEntreprises();

    Entreprise getEntrepriseById(Long id);

    Entreprise createEntreprise(Entreprise entreprise);

    Entreprise updateEntreprise(Long id, Entreprise e);

    void deleteEntreprise(Long id) throws ResourceNotFoundException;

    List<EntrepriseDTO> getEntreprisesDTO();

    ResponseEntity<EntrepriseDTO> getEntrepriseByIdDTO(Long id);

    ResponseEntity<EntrepriseDTO> createEntrepriseDTO(EntrepriseDTO entrepriseDTO);

    ResponseEntity<EntrepriseDTO> updateEntrepriseDTO(Long id, EntrepriseDTO entrepriseDTO);

    void  deleteEntrepriseDTO(Long id);
}
