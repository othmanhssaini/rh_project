package com.example.RH_PROJECT.Service;
import com.example.RH_PROJECT.DTO.UserDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Conge;
import com.example.RH_PROJECT.Model.User;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface IUserService {
    List<User> getUsers();

    User getUserById(Long id);

    User createUser(User user);

    User updateUser(Long id, User u);

    void deleteUser(Long id) throws ResourceNotFoundException;

    List<UserDTO> getUsersDTO();

    ResponseEntity<UserDTO> getUsersByIdDTO(Long id);

    ResponseEntity<UserDTO> createUserDTO(UserDTO userDTO);

    User addUserToDepartement(Long departementId, Long userId);

    ResponseEntity<UserDTO> updateUserDTO(Long id, UserDTO userDTO);

    void  deleteUserDTO(Long id);

    List<User> getUsersDisponibles();

    List<User> getManagerDisponibles();

    User desaffectManager(Long id, User u);

    User getUserInfo();
}
