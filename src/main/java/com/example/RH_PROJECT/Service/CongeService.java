package com.example.RH_PROJECT.Service;
import com.example.RH_PROJECT.DAO.CongeRepository;
import com.example.RH_PROJECT.DTO.CongeDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Conge;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CongeService implements ICongeService{

    @Autowired
    private final CongeRepository congeRepository ;

    @Autowired
    private ModelMapper modelMapper ;

    public CongeService(CongeRepository congeRepository) {
        this.congeRepository = congeRepository;
    }

    @Override
    public List<Conge> getConges() {
        return congeRepository.findAll();
    }

    @Override
    public Conge getCongeById(Long id) {
        return congeRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Conge not found ::" +id));
    }

    @Override
    public Conge createConge(Conge conge) {
        return congeRepository.save(conge);
    }

    @Override
    public Conge updateConge(Long id, Conge c)  {
        Conge conge = congeRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Conge not found::" +id));
        conge.setDateDebut(c.getDateDebut());
        conge.setDateFin(c.getDateFin());
        conge.setMotif(c.getMotif());
        conge.setDuree(c.getDuree());
        conge.setStatus(c.getStatus());
        return congeRepository.save(conge);
    }

    @Override
    public void deleteConge(Long id) {
        Conge conge = congeRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Conge not found ::"+id));
        congeRepository.delete(conge);
    }

    @Override
    public List<CongeDTO> getCongesDTO(){
        return this.getConges()
                .stream()
                .map(conge -> modelMapper.map(conge,CongeDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<CongeDTO> getCongeByIdDTO(Long id ){
        Conge conge = this.getCongeById(id);
        CongeDTO congeDTO = modelMapper.map(conge,CongeDTO.class);
        return ResponseEntity.ok().body(congeDTO);
    }

    @Override
    public ResponseEntity<CongeDTO> createCongeDTO(CongeDTO congeDTO){
        System.out.println("hellooo" + congeDTO);
        Conge congeRequest = modelMapper.map(congeDTO , Conge.class);
        Conge conge = this.createConge(congeRequest);
        CongeDTO congeResponse = modelMapper.map(conge , CongeDTO.class);
        return new ResponseEntity<>(congeResponse, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<CongeDTO> updateCongeDTO(Long id , CongeDTO congeDTO)  {
        Conge congeRequest = modelMapper.map(congeDTO , Conge.class);
        Conge conge = this.updateConge(id , congeRequest);
        CongeDTO congeResponse = modelMapper.map(conge,CongeDTO.class);
        return ResponseEntity.ok().body(congeResponse);
    }

    @Override
    public void deleteCongeDTO(long id){
        this.deleteConge(id);
    }
}
