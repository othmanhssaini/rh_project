package com.example.RH_PROJECT.Service;

import com.example.RH_PROJECT.DTO.CongeDTO;
import com.example.RH_PROJECT.Model.Conge;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.List;

public interface ICongeService {

    public List<Conge> getConges();

    Conge getCongeById(Long id);

    Conge createConge(Conge conge);

    Conge updateConge(Long id , Conge conge) throws ParseException;

    void deleteConge(Long id );

    List<CongeDTO> getCongesDTO();

    ResponseEntity<CongeDTO> getCongeByIdDTO(Long id);

    ResponseEntity<CongeDTO> createCongeDTO(CongeDTO congeDTO);

    ResponseEntity<CongeDTO> updateCongeDTO(Long id, CongeDTO congeDTO) ;

    void deleteCongeDTO(long id);
}
