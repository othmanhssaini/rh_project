package com.example.RH_PROJECT.Service;

import com.example.RH_PROJECT.DAO.DemandeRepository;
import com.example.RH_PROJECT.DAO.UserRepository;
import com.example.RH_PROJECT.DTO.DemandeDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Demande;
import com.example.RH_PROJECT.Model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DemandeService implements IDemandeService {

  @Autowired
  private final DemandeRepository demandeRepository;

  @Autowired
  private final UserRepository userRepository;

  @Autowired
  private ModelMapper modelMapper ;

  public DemandeService(DemandeRepository demandeRepository, UserRepository userRepository) {
    this.demandeRepository = demandeRepository;
    this.userRepository = userRepository;
  }

  @Override
  public List<Demande> getDemandes(){
    return demandeRepository.findAll();
  }

  @Override
  public List<DemandeDTO> getDemandesDTO(){
    return this.getDemandes().stream()
      .map(demande -> modelMapper.map(demande,DemandeDTO.class))
      .collect(Collectors.toList());
  }

  @Override
  public Demande getDemandeById(Long id){
    return demandeRepository.findById(id).
    orElseThrow(()-> new ResourceNotFoundException("Demande not found on :: " +id));
  }

  @Override
  public ResponseEntity<DemandeDTO> getDemandeByIdDTO(Long id){
    Demande demande = this.getDemandeById(id);
    DemandeDTO demandeDTO = modelMapper.map(demande, DemandeDTO.class);
    return ResponseEntity.ok().body(demandeDTO);
  }

  @Override
  public Demande createDemande(Demande demande){
    return demandeRepository.save(demande);
  }

  @Override
  public Demande updateDemande(Long id, Demande d){
    Demande demande = demandeRepository.findById(id).
      orElseThrow(()-> new ResourceNotFoundException("Demande not found on :: " +id));
    demande.setDateDemande(d.getDateDemande());
    demande.setType(d.getType());
    demande.setStatus(d.getStatus());
    return demandeRepository.save(demande);
  }

  @Override
  public void deleteDemande(Long id){
    Demande d = demandeRepository.findById(id).
      orElseThrow(()-> new ResourceNotFoundException("Demande not found on :: " +id));
    demandeRepository.delete(d);
  }

  @Override
  public void deleteDemandeDTO(Long id){
    this.deleteDemande(id);
  }

  @Override
  public Demande addDemandeToUser(Long userId, Long demandeId){
    User user = userRepository.findById(userId).
      orElseThrow(()-> new ResourceNotFoundException("User not found : " +userId) );
    Demande demande = demandeRepository.findById(demandeId).
      orElseThrow(()-> new ResourceNotFoundException("Demande not found on :: " +demandeId));
    demande.addDemandeToUser(user);
    return demandeRepository.save(demande);
  }

  @Override
  public ResponseEntity<DemandeDTO> createDemandeDTO(DemandeDTO demandeDTO){
    Demande demandeRequest = modelMapper.map(demandeDTO, Demande.class);
    Demande demande = this.createDemande(demandeRequest);
    DemandeDTO demandeResponse = modelMapper.map(demande, DemandeDTO.class);
    return new ResponseEntity<>(demandeResponse , HttpStatus.CREATED);
  }

  @Override
  public ResponseEntity<DemandeDTO> updateDemandeDTO(Long id, DemandeDTO demandeDTO){
    Demande demandeRequest = modelMapper.map(demandeDTO , Demande.class);
    Demande demande = this.updateDemande(id,demandeRequest);
    DemandeDTO demandeResponse = modelMapper.map(demande, DemandeDTO.class);
    return ResponseEntity.ok().body(demandeResponse);
  }
}
