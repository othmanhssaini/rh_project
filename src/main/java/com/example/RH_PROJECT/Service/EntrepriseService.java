package com.example.RH_PROJECT.Service;
import com.example.RH_PROJECT.DAO.EntrepriseRepository;
import com.example.RH_PROJECT.DTO.EntrepriseDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Entreprise;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EntrepriseService implements IEntrepriseService{

    @Autowired
    private final EntrepriseRepository entrepriseRepository;

    @Autowired
    private ModelMapper modelMapper ;

    public EntrepriseService(EntrepriseRepository entrepriseRepository) {
        this.entrepriseRepository = entrepriseRepository;
    }

    @Override
    public List<Entreprise> getEntreprises() {
        return entrepriseRepository.findAll();
    }

    @Override
    public Entreprise getEntrepriseById(Long id) {
        return entrepriseRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("Entreprise not found on :: " +id));
    }

    @Override
    public Entreprise createEntreprise(Entreprise entreprise) {
        return entrepriseRepository.save(entreprise);
    }

    @Override
    public Entreprise updateEntreprise(Long id, Entreprise e) {
        Entreprise entreprise = entrepriseRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("Entreprise not found on :: " +id));
        entreprise.setName(e.getName());
        entreprise.setAdress(e.getAdress());
        entreprise.setEmail(e.getEmail());
        return entrepriseRepository.save(entreprise);
    }

    @Override
    public void deleteEntreprise(Long id) throws ResourceNotFoundException {
        Entreprise entreprise = entrepriseRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("Entreprise not found on :: " +id));
        entrepriseRepository.delete(entreprise);
    }

    @Override
    public List<EntrepriseDTO> getEntreprisesDTO(){
        return this.getEntreprises().stream()
                .map(entreprise -> modelMapper.map(entreprise,EntrepriseDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<EntrepriseDTO> getEntrepriseByIdDTO(Long id){
        Entreprise entreprise = this.getEntrepriseById(id);
        EntrepriseDTO entrepriseDTO = modelMapper.map(entreprise,EntrepriseDTO.class);
        return ResponseEntity.ok().body(entrepriseDTO);
    }

    @Override
    public ResponseEntity<EntrepriseDTO> createEntrepriseDTO(EntrepriseDTO entrepriseDTO){
        Entreprise entrepriseRequest = modelMapper.map(entrepriseDTO,Entreprise.class);
        Entreprise entreprise = this.createEntreprise(entrepriseRequest);
        EntrepriseDTO entrerpiseResponse = modelMapper.map(entreprise , EntrepriseDTO.class);
        return new ResponseEntity<>(entrerpiseResponse , HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<EntrepriseDTO> updateEntrepriseDTO(Long id, EntrepriseDTO entrepriseDTO){
        Entreprise entrepriseRequest = modelMapper.map(entrepriseDTO , Entreprise.class);
        Entreprise entreprise = this.updateEntreprise(id,entrepriseRequest);
        EntrepriseDTO entrepriseResponse = modelMapper.map(entreprise , EntrepriseDTO.class);
        return ResponseEntity.ok().body(entrepriseResponse);
    }

    @Override
    public  void  deleteEntrepriseDTO(Long id){
        this.deleteEntreprise(id);
    }
}
