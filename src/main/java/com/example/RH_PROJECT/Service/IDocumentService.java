package com.example.RH_PROJECT.Service;

import com.example.RH_PROJECT.DTO.DocumentDTO;
import com.example.RH_PROJECT.Model.Document;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IDocumentService {

  List<Document> getDocuments();

  List<DocumentDTO> getDocumentsDTO();

  Document getDocumentById(Long id);

  ResponseEntity<DocumentDTO> getDocumentDTOById(Long id);

  Document createDocument(Document document);

  ResponseEntity<DocumentDTO> createDocumentDTO(DocumentDTO documentDTO);

  Document updateDocument(Long id, Document d);

  ResponseEntity<DocumentDTO> updateDocumentDTO(Long id, DocumentDTO documentDTO);

  void deleteDocument(Long id);

  void deleteDocumentDTO(long id);
}
