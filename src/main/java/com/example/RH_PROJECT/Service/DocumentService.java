package com.example.RH_PROJECT.Service;

import com.example.RH_PROJECT.DAO.DocumentRepository;
import com.example.RH_PROJECT.DTO.DemandeDTO;
import com.example.RH_PROJECT.DTO.DocumentDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Document;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DocumentService implements IDocumentService{

  @Autowired
  private final DocumentRepository documentRepository ;

  public DocumentService(DocumentRepository documentRepository) {
    this.documentRepository = documentRepository;
  }

  @Autowired
  private ModelMapper modelMapper ;

  @Override
  public List<Document> getDocuments(){
    return this.documentRepository.findAll();
  }

  @Override
  public List<DocumentDTO> getDocumentsDTO(){
    return this.getDocuments().stream()
      .map(document -> modelMapper.map(document, DocumentDTO.class))
      .collect(Collectors.toList());
  }

  @Override
  public Document getDocumentById(Long id){
    return documentRepository.findById(id).
      orElseThrow(()-> new ResourceNotFoundException("Document not found on :: " +id));
  }

  @Override
  public ResponseEntity<DocumentDTO> getDocumentDTOById(Long id){
    Document document = this.getDocumentById(id);
    DocumentDTO documentDTO = modelMapper.map(document , DocumentDTO.class);
    return ResponseEntity.ok().body(documentDTO);
  }

  @Override
  public Document createDocument(Document document){
    return documentRepository.save(document);
  }

  @Override
  public ResponseEntity<DocumentDTO> createDocumentDTO(DocumentDTO documentDTO){
    Document documentRequest = modelMapper.map(documentDTO, Document.class);
    Document document = this.createDocument(documentRequest);
    DocumentDTO documentResponse = modelMapper.map(document, DocumentDTO.class);
    return ResponseEntity.ok().body(documentResponse);
  }

  @Override
  public Document updateDocument(Long id, Document d){
    Document document = this.documentRepository.findById(id).
      orElseThrow(()-> new ResourceNotFoundException("Document not found on :: " +id));
    document.setData(d.getData());
    return documentRepository.save(document);
  }

  @Override
  public ResponseEntity<DocumentDTO> updateDocumentDTO(Long id, DocumentDTO documentDTO){
    Document documentRequest = modelMapper.map(documentDTO, Document.class);
    Document document = this.updateDocument(id , documentRequest);
    DocumentDTO documentResponse = modelMapper.map(document, DocumentDTO.class);
    return ResponseEntity.ok().body(documentResponse);
  }

  @Override
  public void deleteDocument(Long id){
    Document document = documentRepository.findById(id).
      orElseThrow(()-> new ResourceNotFoundException("Document not found on :: " +id));
    documentRepository.delete(document);
  }

  @Override
  public void deleteDocumentDTO(long id){
     this.deleteDocument(id);
  }

}
