package com.example.RH_PROJECT.Service;
import com.example.RH_PROJECT.DAO.DepartementRepository;
import com.example.RH_PROJECT.DAO.EntrepriseRepository;
import com.example.RH_PROJECT.DAO.UserRepository;
import com.example.RH_PROJECT.DTO.DepartementDTO;
import com.example.RH_PROJECT.Exception.ResourceNotFoundException;
import com.example.RH_PROJECT.Model.Departement;
import com.example.RH_PROJECT.Model.Entreprise;
import com.example.RH_PROJECT.Model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartementService implements IDepartementService {

    @Autowired
    private final DepartementRepository departementRepository ;

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final EntrepriseRepository entrepriseRepository;

    @Autowired
    private ModelMapper modelMapper ;

    public DepartementService(DepartementRepository departementRepository, UserRepository userRepository, EntrepriseRepository entrepriseRepository) {
        this.departementRepository = departementRepository;
        this.userRepository = userRepository;
        this.entrepriseRepository = entrepriseRepository;
    }

    @Override
    public List<Departement> getDepartements() {
        return departementRepository.findAll();
    }

    @Override
    public Departement getDepartementById(Long id) {
        return departementRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("Departement not found on :: " +id));
    }

    /*
    *  public ResponseEntity<DepartementDTO> getDepartementsByIdDTO(Long id){
        Departement departement = this.getDepartementById(id);
        DepartementDTO departementDTO = modelMapper.map(departement,DepartementDTO.class);
        return ResponseEntity.ok().body(departementDTO);
    }*/

    @Override
    public Departement createDepartement(Departement departement) {
        return departementRepository.save(departement);
    }

    @Override
    public Departement updateDepartement(Long id, Departement d) {
        Departement departement = departementRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("Departement not found on :: " +id));
        departement.setName(d.getName());
        departement.setPlace(d.getPlace());
        return departementRepository.save(departement);
    }

    @Override
    public void deleteDepartement(Long id) throws ResourceNotFoundException {
        Departement departement = departementRepository.findById(id).
                orElseThrow(()-> new ResourceNotFoundException("Departement not found on :: " +id));
        departementRepository.delete(departement);
    }

    @Override
    public Departement assignEntrepriseToDepartement(Long departementId, Long entrepriseId)  {
        Departement departement = departementRepository.findById(departementId).
                orElseThrow(()-> new ResourceNotFoundException("Departement not found : " +departementId) );
        Entreprise entreprise = entrepriseRepository.findById(entrepriseId).
                orElseThrow(()-> new ResourceNotFoundException("Entreprise not found " +entrepriseId));
        departement.assignEntreprise(entreprise);
        return departementRepository.save(departement);
    }



    @Override
    public Departement addUsersToDepartement(Long departementId, Long[] userIds) {
        Departement departement = departementRepository.findById(departementId).
                orElseThrow(()-> new ResourceNotFoundException("Departement not found : " +departementId) );
        ArrayList<User> users = new ArrayList<User>();
        for (int i =0 ; i<userIds.length ; i++){
            int finalI = i;
            User user = userRepository.findById(userIds[i]).
                    orElseThrow(()-> new ResourceNotFoundException("User not found " +userIds[finalI]));
            users.add(user);
        }

        departement.addUsersToDepartement((User[])users.toArray());
        return departementRepository.save(departement);
    }

    @Override
    public List<DepartementDTO> getDepartementsDTO(){
        return this.getDepartements().stream()
                .map(departement -> modelMapper.map(departement,DepartementDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<DepartementDTO> getDepartementsByIdDTO(Long id){
        Departement departement = this.getDepartementById(id);
        DepartementDTO departementDTO = modelMapper.map(departement,DepartementDTO.class);
        return ResponseEntity.ok().body(departementDTO);
    }

    @Override
    public ResponseEntity<DepartementDTO> createDepartementDTO(DepartementDTO departementDTO){
        Departement departementRequest = modelMapper.map(departementDTO,Departement.class);
        Departement departement = this.createDepartement(departementRequest);
        DepartementDTO departementResponse = modelMapper.map(departement , DepartementDTO.class);
        return new ResponseEntity<>(departementResponse , HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DepartementDTO> updateDepartementDTO(Long id, DepartementDTO departementDTO){
        Departement departementRequest = modelMapper.map(departementDTO , Departement.class);
        Departement departement = this.updateDepartement(id,departementRequest);
        DepartementDTO departementResponse = modelMapper.map(departement , DepartementDTO.class);
        return ResponseEntity.ok().body(departementResponse);
    }

    @Override
    public  void  deleteDepatementDTO(Long id){
        this.deleteDepartement(id);
    }
}
