package com.example.RH_PROJECT.Exception;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalException {

    @ExceptionHandler(ResourceNotFoundException.class)

    public ResponseEntity<ErrorObject> handleResourceNotFoundException (ResourceNotFoundException exception){
        ErrorObject errorObject = new ErrorObject();
        errorObject.setStatus(HttpStatus.NOT_FOUND.value());
        errorObject.setMessage(exception.getMessage());
        errorObject.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<ErrorObject>(errorObject,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<ErrorObject> handleNoDataFoundException (NoDataFoundException exception){
        ErrorObject errorObject = new ErrorObject();
        errorObject.setStatus(HttpStatus.NO_CONTENT.value());
        errorObject.setMessage(exception.getMessage());
        errorObject.setTimestamp(System.currentTimeMillis());
        return  new ResponseEntity<ErrorObject>(errorObject , HttpStatus.OK);
    }

    @ExceptionHandler(APIException.class)
    public ResponseEntity<ErrorObject> handleAPIException (APIException exception){
        ErrorObject errorObject = new ErrorObject();
        errorObject.setStatus(HttpStatus.NO_CONTENT.value());
        errorObject.setMessage(exception.getMessage());
        errorObject.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<ErrorObject>(errorObject,HttpStatus.NOT_FOUND);
    }
}
