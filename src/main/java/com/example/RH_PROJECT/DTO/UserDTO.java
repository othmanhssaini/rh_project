package com.example.RH_PROJECT.DTO;
import com.example.RH_PROJECT.Enum.UserEnum;
import com.example.RH_PROJECT.Model.Departement;
import java.util.Date;

public class UserDTO {

    private long id ;

    private String firstName ;

    private String lastName ;

    private Date dob ;

    private Date dateEntree ;

    private Date dateSortie ;

    private Integer age ;

    private String email ;

    private String phoneNumber ;

    private String address ;

    private UserEnum.USER_ROLE userRole;

    private Departement departement;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public UserEnum.USER_ROLE getUserRole() {
    return userRole;
  }

    public void setUserRole(UserEnum.USER_ROLE userRole) {
    this.userRole = userRole;
  }

    public Date getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }

    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }
}
