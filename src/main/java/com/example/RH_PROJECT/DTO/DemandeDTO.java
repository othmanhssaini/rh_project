package com.example.RH_PROJECT.DTO;

import com.example.RH_PROJECT.Enum.DemandeEnum;
import com.example.RH_PROJECT.Model.Document;
import com.example.RH_PROJECT.Model.User;

import java.util.Date;

public class DemandeDTO {

  private long id ;

  private Date dateDemande ;

  private DemandeEnum.demande_Type type ;

  private DemandeEnum.demande_Status status;

  private Document document ;

  private User user ;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Date getDateDemande() {
    return dateDemande;
  }

  public void setDateDemande(Date dateDemande) {
    this.dateDemande = dateDemande;
  }

  public DemandeEnum.demande_Type getType() {
    return type;
  }

  public void setType(DemandeEnum.demande_Type type) {
    this.type = type;
  }

  public DemandeEnum.demande_Status getStatus() {
    return status;
  }

  public void setStatus(DemandeEnum.demande_Status status) {
    this.status = status;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Document getDocument() {
    return document;
  }

  public void setDocument(Document document) {
    this.document = document;
  }
}
