package com.example.RH_PROJECT.DTO;

import com.example.RH_PROJECT.Enum.DocumentEnum;
import com.example.RH_PROJECT.Model.Demande;

public class DocumentDTO {

  private Long Id ;

  private Byte[] data ;

  private Demande demande ;

  public Long getId() {
    return Id;
  }

  public void setId(Long id) {
    Id = id;
  }

  public Byte[] getData() {
    return data;
  }

  public void setData(Byte[] data) {
    this.data = data;
  }

  public Demande getDemande() {
    return demande;
  }

  public void setDemande(Demande demande) {
    this.demande = demande;
  }
}
