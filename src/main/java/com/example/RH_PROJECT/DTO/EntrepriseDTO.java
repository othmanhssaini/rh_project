package com.example.RH_PROJECT.DTO;
import com.example.RH_PROJECT.Model.Departement;
import java.util.LinkedList;
import java.util.List;

public class EntrepriseDTO {

    private long id ;

    private String name ;

    private String address ;

    private String email ;

    private List<Departement> departements = new LinkedList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Departement> getDepartements() {
        return departements;
    }

    public void setDepartements(List<Departement> departements) {
        this.departements = departements;
    }
}
