package com.example.RH_PROJECT.DTO;
import com.example.RH_PROJECT.Enum.CongeEnum;
import com.example.RH_PROJECT.Model.User;
import java.util.Date;

public class CongeDTO {

    private long id ;

    private Date dateDebut;

    private Date dateFin;

    private String motif ;

    private Long duree ;

    private CongeEnum.Status status ;

    private User user ;

    public long getId(){
        return id ;
    }

    public void setId(long id) {
        this.id = id ;
    }

    public Date getDateDebut()  {
        return  dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin()  {
        return  dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public Long getDuree() {
        return DateUtils.getDuree(dateDebut,dateFin);
    }

    public void setDuree(Long duree) {
        this.duree = duree;
    }

    public CongeEnum.Status getStatus() {
        return status;
    }

    public void setStatus(CongeEnum.Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
