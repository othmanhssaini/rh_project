package com.example.RH_PROJECT.DTO;
import java.io.Serializable;
import java.util.Date;

public class DateUtils implements Serializable {

    public static final String PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static Long getDuree(Date dateDebut , Date dateFin){
        long duree = 0;
        if(dateFin.after(dateDebut)){
            duree = dateFin.getTime() - dateDebut.getTime();
        }
        return (duree/(1000*60*60*24)) ;
    }
}
