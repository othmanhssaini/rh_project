package com.example.RH_PROJECT.DTO;

public class AffectUsers {

    Long[] userIds;

    public AffectUsers() {
    }

    public AffectUsers(Long[] userIds) {
        this.userIds = userIds;
    }

    public Long[] getUserIds() {
        return userIds;
    }

    public void setUserIds(Long[] userIds) {
        this.userIds = userIds;
    }
}
