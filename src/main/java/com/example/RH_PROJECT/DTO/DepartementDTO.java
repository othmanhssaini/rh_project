package com.example.RH_PROJECT.DTO;
import com.example.RH_PROJECT.Model.Entreprise;
import com.example.RH_PROJECT.Model.User;
import java.util.LinkedList;
import java.util.List;

public class DepartementDTO {

    private long id ;

    private String name ;

    private String place ;

    private List<User> users = new LinkedList<>();

    private Entreprise entreprise ;

    private User manager ;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }
}
