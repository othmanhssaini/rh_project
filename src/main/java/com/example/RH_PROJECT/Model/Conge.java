package com.example.RH_PROJECT.Model;
import com.example.RH_PROJECT.DTO.DateUtils;
import com.example.RH_PROJECT.Enum.CongeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
public class Conge  extends BaseEntity implements Serializable {

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "date_debut")
    private Date dateDebut;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "date_fin")
    private Date dateFin;

    private String motif ;

    private Long duree ;

    @Enumerated(EnumType.ORDINAL)
    private CongeEnum.Status status ;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id" , referencedColumnName = "id")
    private User user ;

    public Conge() {
    }

    public Conge(Date dateDebut, Date dateFin, String motif, Long duree, CongeEnum.Status status, User user) {
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.motif = motif;
        this.duree = duree;
        this.status = status;
        this.user = user;
    }

    public Date getDateDebut()  {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;

    }
    public Date getDateFin()  {
        return  dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public Long getDuree() {
        return DateUtils.getDuree(dateDebut,dateFin);
    }

    public void setDuree(Long duree) {
        this.duree = duree;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CongeEnum.Status getStatus() {
        return status;
    }

    public void setStatus(CongeEnum.Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Conge{" +
                "dateDebut='" + dateDebut + '\'' +
                ", dateFin='" + dateFin + '\'' +
                ", motif='" + motif + '\'' +
                ", duree=" + duree +
                ", status=" + status +
                ", user=" + user +
                '}';
    }
}
