package com.example.RH_PROJECT.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.example.RH_PROJECT.Enum.UserEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
public class User extends BaseEntity implements Serializable {

    //static final  int INIT_SOLDE = 18 ;

    private String firstName ;

    private String lastName ;

    @Enumerated(EnumType.ORDINAL)
    private UserEnum.USER_ROLE userRole;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name="dob")
    private Date dob ;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name="dateEntree")
    private Date dateEntree ;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name="dateSortie")
    private Date dateSortie ;

    private Integer age ;

    private String email ;

    private String phoneNumber ;

    private String address ;

    @JsonIgnore
    @ManyToMany(mappedBy = "users")
    private List<Departement> departementSet = new LinkedList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "departement_id" , referencedColumnName = "id")
    private Departement departement;

    @OneToMany(mappedBy="user")
    private List<Demande> demandes = new LinkedList<>();

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    public User() {
    }

    public User(String firstName, String lastName, UserEnum.USER_ROLE userRole, Date dob, Date dateEntree, Date dateSortie, Integer age, String email, String phoneNumber, String address, Departement departement, List<Demande> demandes) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userRole = userRole;
        this.dob = dob;
        this.dateEntree = dateEntree;
        this.dateSortie = dateSortie;
        this.age = age;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.departement = departement;
        this.demandes = demandes;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public UserEnum.USER_ROLE getUserRole() {
        return userRole;
    }

    public void setUserRole(UserEnum.USER_ROLE userRole) {
        this.userRole = userRole;
    }

    public Date getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }

    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    public void addUserToDepartement(Departement departement){
    this.departement = departement;
     }



  public List<Demande> getDemandes() {
    return demandes;
  }

  public void setDemandes(List<Demande> demandes) {
    this.demandes = demandes;
  }

  @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dob='" + dob + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", departement=" + departement +
                '}';
    }

}
