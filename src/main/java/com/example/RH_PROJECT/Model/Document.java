package com.example.RH_PROJECT.Model;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "document")
public class Document extends BaseEntity implements Serializable {

  @Column(name = "bits")
  private Byte[] data ;


  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "demande_id", referencedColumnName = "id")
  private Demande demande ;

  public Document() {
  }

  public Document(Byte[] data, Demande demande) {
    this.data = data;
    this.demande = demande;
  }

  public Byte[] getData() {
    return data;
  }

  public void setData(Byte[] data) {
    this.data = data;
  }


  public Demande getDemande() {
    return demande;
  }

  public void setDemande(Demande demande) {
    this.demande = demande;
  }

  @Override
  public String toString() {
    return "Document{" +
      "Data=" + data +
      ", demande=" + demande +
      '}';
  }
}
