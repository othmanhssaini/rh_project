package com.example.RH_PROJECT.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
public class Departement extends BaseEntity implements Serializable {

  private String Name ;

  private String place ;

  @JsonIgnore
  @OneToMany(mappedBy = "departement")
  private List<User> users = new LinkedList<>();


  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "manager_fk")
  private User manager ;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "entreprise_id" , referencedColumnName = "id")
  private Entreprise entreprise ;

  public Departement() {
  }

  public Departement(String Name, String place) {
      this.Name = Name;
      this.place = place;
  }

  public Departement(String name, String place, List<User> users, User manager, Entreprise entreprise) {
    Name = name;
    this.place = place;
    this.users = users;
    this.manager = manager;
    this.entreprise = entreprise;
  }
  
  public String getName() {
        return Name;
    }

  public void setName(String name) {
      Name = name;
  }

  public String getPlace() {
      return place;
  }

  public void setPlace(String place) {
      this.place = place;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public void setEntreprise(Entreprise entreprise) {
    this.entreprise = entreprise;
  }

  public Entreprise getEntreprise() {
        return entreprise;
    }

  public void addUsersToDepartement(User[] users){
      this.users.addAll(Arrays.asList(users));
  }

  public void assignEntreprise(Entreprise entreprise) {
      this.entreprise = entreprise;
  }

  public User getManager() {
    return manager;
  }

  public void setManager(User manager) {
    this.manager = manager;
  }

  @Override
  public String toString() {
      return "Departement{" +
              "Name='" + Name + '\'' +
              ", place='" + place + '\'' +
              ", users=" + users +
              '}';
  }
}
