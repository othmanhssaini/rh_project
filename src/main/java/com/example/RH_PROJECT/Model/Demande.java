package com.example.RH_PROJECT.Model;

import com.example.RH_PROJECT.Enum.DemandeEnum;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "demande")
public class Demande extends BaseEntity implements Serializable {

  @Column(name="dateDemande")
  private Date dateDemande ;

  @Enumerated(EnumType.ORDINAL)
  private DemandeEnum.demande_Type type ;

  @Enumerated(EnumType.ORDINAL)
  private DemandeEnum.demande_Status status;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "user_id" , referencedColumnName = "id")
  private User user ;

  @OneToOne(mappedBy = "demande")
  private Document document ;

  public Demande() {
  }

  public Demande(Date dateDemande, DemandeEnum.demande_Type type, DemandeEnum.demande_Status status, User user, Document document) {
    this.dateDemande = dateDemande;
    this.type = type;
    this.user = user;
    this.document = document ;
    this.status = status ;
  }

  public Date getDateDemande() {
    return dateDemande;
  }

  public void setDateDemande(Date dateDemande) {
    this.dateDemande = dateDemande;
  }

  public DemandeEnum.demande_Type getType() {
    return type;
  }

  public void setType(DemandeEnum.demande_Type type) {
    this.type = type;
  }

  public DemandeEnum.demande_Status getStatus() {
    return status;
  }

  public void setStatus(DemandeEnum.demande_Status status) {
    this.status = status;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Document getDocument() {
    return document;
  }

  public void setDocument(Document document) {
    this.document = document;
  }

  public void addDemandeToUser(User user){
    this.user = user;
  }

  @Override
  public String toString() {
    return "Demande{" +
      "dateDemande='" + dateDemande + '\'' +
      ", status=" + type +
      ", user=" + user +
      ",document=" + document +
      ",status=" + status +
      '}';
  }
}
