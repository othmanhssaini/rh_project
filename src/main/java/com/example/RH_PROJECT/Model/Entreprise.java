package com.example.RH_PROJECT.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
public class Entreprise extends BaseEntity implements Serializable {

    private String name ;

    private String adress ;

    private String email ;

    @JsonIgnore
    @OneToMany(mappedBy = "entreprise")
    private List<Departement> departements = new LinkedList<>();

    public Entreprise() {
    }

    public Entreprise(String name, String adress, String email) {
        this.name = name;
        this.adress = adress;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Departement> getDepartements() {
        return departements;
    }

    @Override
    public String toString() {
        return "Entreprise{" +
                "name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
