package com.example.RH_PROJECT.Configuration;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationDTO {
    
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
