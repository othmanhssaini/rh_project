package com.example.RH_PROJECT.Enum;
import java.util.stream.Stream;

public class UserEnum {
    
    public  enum USER_ROLE {
        RH(0),
        MANAGER(1),
        USER(2);

        private final int number;

        private USER_ROLE(int number) {
            this.number = number;
        }

        public int getNumber() {
            return this.number;
        }

        @Override
        public String toString() {
            return Integer.toString(this.number);
        }
        public static USER_ROLE of(int number) {
            return Stream.of(USER_ROLE.values())
                    .filter(g -> g.getNumber() == number)
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid grade number: " + number));
        }
    }
}
