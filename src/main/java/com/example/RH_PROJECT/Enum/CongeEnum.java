package com.example.RH_PROJECT.Enum;

import java.util.stream.Stream;

public class CongeEnum {

    public enum Status {
        ACCEPTE (0),
        ENCOURS (1),
        ANNULE (2) ;

        private final int number;

        Status(int number) {
            this.number = number;
        }

        public int getNumber() {
            return this.number;
        }

        @Override
        public String toString() {
            return Integer.toString(this.number);
        }
        public static CongeEnum.Status of(int number) {
            return Stream.of(CongeEnum.Status.values())
                    .filter(g -> g.getNumber() == number)
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid grade number: " + number));
        }
    }
}
