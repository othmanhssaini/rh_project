package com.example.RH_PROJECT.Enum;

import java.util.stream.Stream;

public class DemandeEnum {

  public enum demande_Type{
    BULLETINPAIE(0),
    ATTESTATION(1);

  private final int number;

  private demande_Type(int number) {
      this.number = number;
    }

  public int getNumber(){ return this.number;}

  @Override
  public String toString() {
    return Integer.toString(this.number);
  }

  public static DemandeEnum.demande_Type of(int number) {
    return Stream.of(DemandeEnum.demande_Type.values())
      .filter(g -> g.getNumber() == number)
      .findFirst()
      .orElseThrow(() -> new IllegalArgumentException("Invalid grade number: " + number));
  }
  }
  public enum demande_Status{
    ENCOURS(0),
    APPROUVE(1);


    private final int numberStatus ;

    private demande_Status(int numberStatus){this.numberStatus = numberStatus ;}

    public int getNumberStatus(){ return this.numberStatus;}

    @Override
    public String toString() {
      return Integer.toString(this.numberStatus);
    }

    public static DemandeEnum.demande_Status of(int numberStatus) {
      return Stream.of(DemandeEnum.demande_Status.values())
        .filter(g -> g.getNumberStatus() == numberStatus)
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("Invalid grade number: " + numberStatus));
    }
  }
}

