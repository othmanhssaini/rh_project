package com.example.RH_PROJECT.DAO;
import com.example.RH_PROJECT.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  public Optional<User> findByEmail(String email);

  @Query(value = "select * FROM User u where u.departement_id IS NULL and u.user_role != 1 " , nativeQuery = true)
  List<User> getUsersDisponibles();

  @Query(value = "SELECT * FROM User u WHERE  u.departement_id IS NULL and u.user_role = 1",nativeQuery = true)
  List<User> getManagerDisponibles();
}


