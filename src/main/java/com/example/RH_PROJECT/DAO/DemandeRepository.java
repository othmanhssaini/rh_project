package com.example.RH_PROJECT.DAO;

import com.example.RH_PROJECT.Model.Demande;
import com.example.RH_PROJECT.Model.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.metamodel.ListAttribute;
import java.util.List;

@Repository
public interface DemandeRepository extends JpaRepository<Demande,Long> {

}
